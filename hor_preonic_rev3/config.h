#pragma once

#define FORCE_NKRO
#define AUDIO_CLICKY
#define AUDIO_CLICKY_FREQ_RANDOMNESS 0.1f
#define MIDI_BASIC

#ifdef AUDIO_ENABLE
#define STARTY_SOUND  \
Q__NOTE(_F4),   \
Q__NOTE(_G4),   \
Q__NOTE(_BF4),  \
Q__NOTE(_G4), \
HD_NOTE(_D5), \
S__NOTE(_REST),   \
HD_NOTE(_D5),  \
W__NOTE(_C5),  \
S__NOTE(_REST),   \
S__NOTE(_REST),   \
Q__NOTE(_F4),  \
Q__NOTE(_G4), \
Q__NOTE(_BF4), \
Q__NOTE(_G4),  \
HD_NOTE(_C5),  \
S__NOTE(_REST),   \
HD_NOTE(_C5), \
W__NOTE(_BF4),  \
S__NOTE(_REST),   \
S__NOTE(_REST),   \
Q__NOTE(_F4),  \
Q__NOTE(_G4),  \
Q__NOTE(_BF4), \
Q__NOTE(_G4),  \
W__NOTE(_BF4),  \
H__NOTE(_C5),  \
H__NOTE(_A4), \
H__NOTE(_A4),  \
H__NOTE(_G4), \
H__NOTE(_F4),  \
H__NOTE(_F4), \
W__NOTE(_C5),  \
W__NOTE(_BF4)

    // #define STARTUP_SONG SONG(STARTY_SOUND)
    #define STARTUP_SONG SONG(PREONIC_SOUND)
    // #define STARTUP_SONG SONG(NO_SOUND)

    #define DEFAULT_LAYER_SONGS { SONG(QWERTY_SOUND), \
                                  SONG(COLEMAK_SOUND), \
                                  SONG(DVORAK_SOUND) \
                                }
#endif

#define MUSIC_MASK (keycode != KC_NO)

/*
 * MIDI options
 */

/* Prevent use of disabled MIDI features in the keymap */
//#define MIDI_ENABLE_STRICT 1

/* enable basic MIDI features:
   - MIDI notes can be sent when in Music mode is on
*/

#define MIDI_BASIC

/* enable advanced MIDI features:
   - MIDI notes can be added to the keymap
   - Octave shift and transpose
   - Virtual sustain, portamento, and modulation wheel
   - etc.
*/
//#define MIDI_ADVANCED

/* override number of MIDI tone keycodes (each octave adds 12 keycodes and allocates 12 bytes) */
//#define MIDI_TONE_KEYCODE_OCTAVES 2
