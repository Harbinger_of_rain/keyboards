
# Harbinger's Keyboards

This repository contains the configuration files for my keyboards, as well as a script to clone the qmk repository and running a docker container for building and flashing .hex files.


## Usage

To build and flash a .hex file simply run:
./run.sh -f BOARD
where BOARD is the name or shorthand of the keyboard, for my preonic, this would be
./run.sh -f preonic3

If you intend to clone this repo you're expected to change the script and configuration to fit your own needs.


The script automatically downloads the qmk repository, but doesn't update it (yet?). Keep that in mind when trying to use a new feature.

