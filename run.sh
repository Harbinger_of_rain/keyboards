#!/bin/sh

boardname=
update=false

while [ $# -gt 0 ]
	do
		# get parameter values
	case "$1" in
		-h|--help)	# help
			echo "run.sh BOARDNAME \
			keyboards: preonic3, ergodox-ez"
			;;
		-f|--flash)
			shift  # to get the next parameter
			errorMsg="--- INVALID NAME SPECIFICATION ---"
			boardname=`echo "$1" | tr "[:upper:]" "[:lower:]"`
			case "$boardname" in
				p3|preonic3) boardname="preonic3" ;;
				ez|ergodox-ez) boardname="ergodox-ez" ;;
				*) errMsg "--- boardname=$boardname not in config, manual setup required! ---" ;;
			esac
			;;
		-u|--update)
			update=true
			;;
	esac
	shift	# next option
done

# Require qmk
if ! type qmk > /dev/null; then
	echo "Please install qmk first: https://docs.qmk.fm/#/newbs_getting_started"
	exit 1
fi

# Setup qmk if missing
if [ ! -d ./qmk_firmware ]; then
	qmk setup -H $(pwd)/qmk_firmware
	qmk config user.keymap=Harbinger_of_rain
fi

# Copy Harbinger keymaps
if [ ! -d ./qmk_firmware/keyboards/preonic/keymaps/Harbinger_of_rain ]; then
	qmk new-keymap -kb preonic/rev3
fi
if [ ! -d ./qmk_firmware/keyboards/ergodox_ez/keymaps/Harbinger_of_rain ]; then
	qmk new-keymap -kb ergodox_ez
fi
cp -rf ./hor_preonic_rev3/* ./qmk_firmware/keyboards/preonic/keymaps/Harbinger_of_rain
cp -rf ./hor_ergodox_ez/* ./qmk_firmware/keyboards/ergodox_ez/keymaps/Harbinger_of_rain

cd qmk_firmware

if [ $update = true ]; then
	qmk setup
fi

case "$boardname" in
	preonic3)
		qmk flash -kb preonic/rev3
	;;
	ergodox-ez)
		qmk flash -kb ergodox_ez
	;;
esac


